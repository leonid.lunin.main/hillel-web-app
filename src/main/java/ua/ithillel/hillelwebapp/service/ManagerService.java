package ua.ithillel.hillelwebapp.service;

import ua.ithillel.hillelwebapp.dao.ManagerDAO;
import ua.ithillel.hillelwebapp.entity.Manager;

import java.util.List;

public class ManagerService {
    private final ManagerDAO managerDAO = new ManagerDAO();

    public List<Manager> getAllManagers() {
        return managerDAO.findAll();
    }
}

