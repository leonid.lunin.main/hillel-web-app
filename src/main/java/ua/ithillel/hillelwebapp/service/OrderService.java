package ua.ithillel.hillelwebapp.service;

import ua.ithillel.hillelwebapp.dao.OrderDAO;
import ua.ithillel.hillelwebapp.entity.Order;

import java.util.List;

public class OrderService {
    private final OrderDAO orderDAO = new OrderDAO();

    public List<Order> getAllOrders() {
        return orderDAO.findAll();
    }

    public void save(Order order) {
        orderDAO.save(order);
    }

    public Order update(Order order) {
        return orderDAO.update(order);
    }

    public void delete(Order order) {
        orderDAO.delete(order);
    }

    public Order findById(int id) {
        return orderDAO.findById(id);
    }
}

