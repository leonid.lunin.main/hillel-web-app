package ua.ithillel.hillelwebapp.service;

import ua.ithillel.hillelwebapp.dao.CarDAO;
import ua.ithillel.hillelwebapp.entity.Car;

import java.util.List;

public class CarService {
    private final CarDAO carDAO = new CarDAO();
    public List<Car> getAllCars(){
        return carDAO.findAll();
    }

    public void save(Car car) {
        carDAO.save(car);
    }

    public Car update(Car car) {
        return carDAO.update(car);
    }

    public void delete(Car car) {
        carDAO.delete(car);
    }

    public Car findById(int id) {
        return carDAO.findById(id);
    }
}

