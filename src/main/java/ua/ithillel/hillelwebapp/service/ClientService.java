package ua.ithillel.hillelwebapp.service;

import ua.ithillel.hillelwebapp.dao.ClientDAO;
import ua.ithillel.hillelwebapp.entity.Client;

import java.util.List;

public class ClientService {
    private final ClientDAO clientDAO = new ClientDAO();

    public List<Client> getAllClients() {
        return clientDAO.findAll();
    }

    public void save(Client client) {
        clientDAO.save(client);
    }

    public Client update(Client client) {
        return clientDAO.update(client);
    }

    public void delete(Client client) {
        clientDAO.delete(client);
    }

    public Client findById(int id) {
        return clientDAO.findById(id);
    }
}

