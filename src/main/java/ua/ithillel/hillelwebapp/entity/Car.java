package ua.ithillel.hillelwebapp.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "cars")
public class Car {
    @Id
    private int id;
    private String manufacturer;
    private String model;
    private int year;
    private int price;
    private boolean available;
}
