package ua.ithillel.hillelwebapp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "managers")
public class Manager {
    @Id
    private int id;
    private String name;
    private String login;
    private String password;
    private String email;
    @Enumerated
    private Role role;

    @OneToMany
    @JsonIgnore
    @JoinColumn(name = "manager_id")
    private List<Order> orders;
}
