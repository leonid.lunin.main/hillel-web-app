package ua.ithillel.hillelwebapp.servlet;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ua.ithillel.hillelwebapp.entity.Order;
import ua.ithillel.hillelwebapp.service.OrderService;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "getOrders", urlPatterns = "/orders")
public class OrderServlet extends HttpServlet {

    OrderService orderService = new OrderService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Order> orders = orderService.getAllOrders();
        req.setAttribute("ordersList", orders);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("orders.jsp");
        requestDispatcher.forward(req, resp);
    }
}
