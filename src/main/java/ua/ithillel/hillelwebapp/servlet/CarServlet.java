package ua.ithillel.hillelwebapp.servlet;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ua.ithillel.hillelwebapp.entity.Car;
import ua.ithillel.hillelwebapp.service.CarService;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "getCars", urlPatterns = "/cars")
public class CarServlet extends HttpServlet {

    private final CarService carService = new CarService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Car> cars = carService.getAllCars();
        req.setAttribute("carsList", cars);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("cars.jsp");
        requestDispatcher.forward(req, resp);
    }
}
