package ua.ithillel.hillelwebapp.servlet;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ua.ithillel.hillelwebapp.entity.Manager;
import ua.ithillel.hillelwebapp.service.ManagerService;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "getManagers", urlPatterns = "/managers")
public class ManagerServlet extends HttpServlet {

    ManagerService managerService = new ManagerService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Manager> managers = managerService.getAllManagers();
        req.setAttribute("managersList", managers);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("managers.jsp");
        requestDispatcher.forward(req, resp);
    }
}
