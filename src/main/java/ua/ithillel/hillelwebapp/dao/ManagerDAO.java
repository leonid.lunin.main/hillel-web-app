package ua.ithillel.hillelwebapp.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ua.ithillel.hillelwebapp.db.HibernateSessionFactory;
import ua.ithillel.hillelwebapp.entity.Manager;

import java.util.List;

public class ManagerDAO implements AbstractDAO<Manager> {
    @Override
    public Manager findById(int id) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Manager manager = session.get(Manager.class, id);
        session.close();
        return manager;
    }

    @Override
    public void save(Manager entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.persist(entity);
        tx1.commit();
        session.close();
    }

    @Override
    public Manager update(Manager entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        Manager manager = session.merge(entity);
        tx1.commit();
        session.close();
        return manager;
    }

    @Override
    public void delete(Manager entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.remove(entity);
        tx1.commit();
        session.close();
    }

    @Override
    public List<Manager> findAll() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        List<Manager> managers = session.createQuery("FROM Manager ", Manager.class).getResultList();
        session.close();
        return managers;
    }
}
