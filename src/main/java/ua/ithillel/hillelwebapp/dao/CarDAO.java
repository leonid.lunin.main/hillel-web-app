package ua.ithillel.hillelwebapp.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ua.ithillel.hillelwebapp.db.HibernateSessionFactory;
import ua.ithillel.hillelwebapp.entity.Car;

import java.util.List;

public class CarDAO implements AbstractDAO<Car> {
    @Override
    public Car findById(int id) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Car car = session.get(Car.class, id);
        session.close();
        return car;
    }

    @Override
    public void save(Car entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.persist(entity);
        tx1.commit();
        session.close();
    }

    @Override
    public Car update(Car entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        Car updatedCar = session.merge(entity);
        tx1.commit();
        session.close();
        return updatedCar;
    }

    @Override
    public void delete(Car entity) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.remove(entity);
        tx1.commit();
        session.close();
    }

    @Override
    public List<Car> findAll() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        List<Car> cars = session.createQuery("FROM Car ", Car.class).getResultList();
        session.close();
        return cars;
    }
}
