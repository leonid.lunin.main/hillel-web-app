package ua.ithillel.hillelwebapp.dao;

import java.util.List;

public interface AbstractDAO<T> {
    T findById(int id);

    void save(T entity);

    T update(T entity);

    void delete(T entity);

    List<T> findAll();
}
