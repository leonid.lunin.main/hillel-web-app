<%@ page import="ua.ithillel.hillelwebapp.entity.Manager" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
    <title>Cars</title>
</head>

<body>
<div style="text-align: center;">
    <h1 class="display-3">Managers</h1>
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Login</th>
                <th>Password</th>
                <th>Email</th>
                <th>Role</th>
            </tr>
            </thead>
            <tbody>
            <%
                List<Manager> list = (List) request.getAttribute("managersList");
            %>
            <%
                for (Manager c : list) {
            %>
            <tr>
                <td><%=c.getId()%>
                </td>
                <td><%=c.getName()%>
                </td>
                <td><%=c.getLogin()%>
                </td>
                <td><%=c.getPassword()%>
                </td>
                <td><%=c.getEmail()%>
                </td>
                <td><%=c.getRole()%>
                </td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>